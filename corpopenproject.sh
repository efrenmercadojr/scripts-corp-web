# How to use:

# Give permission
# chmod +x corpopenproject.sh

# Run the script
# . ./corpopenproject.sh


# Mount drive and navigate to project dir
# Only needed if your workspace is in another drive/partition
sudo mount /dev/sda8 /media/ren/Data
# Change to your own directory
cd /media/ren/Data/projects/vo-corp-web
pwd

echo "Starting mongod service..."
sudo service mongod start

echo "Starting redis on port 6379..."
# Change to redis-server if you are not using redis from docker
sudo docker run -p 6379:6379 -d redis:2.8

echo "Switching to virtual env..."
source venv/bin/activate

echo "Setting aliases... Use 'rs' to run server"
alias rs='python manage.py runserver'
alias mm='python manage.py makemigrations'
alias mg='python manage.py migrate'

echo "Starting IDE"
# Change command depending on your IDE/Text Editor
code .
